############################################################################################################################
# ppip: A Polyploid Phylogenetic Inference Pipeline
# M. Szenteczki
#
# Description: A pipeline for processing raw NGS reads 
# (i.e. Illumina RNAseq) into summative supertrees
#
# Typical Usage:
# 1) phthon ppip.py -p params.txt -s 123		# assemble your raw reads, get QC stats (N50, BUSCO, RSEM-EVAL)
# 2) [annotate the output, move to data_out]	# carrried out independent of pipeline
# 3) python ppip.py -p params.txt -s 4567		# identify orthologs, cluster them, and produce MRP supertrees
# 4) python ppip.py -p params.txt -s 7			# repeat .nex file generation for different cluster #s, after running S1-S6
#
# Summaries of what is run in each step are available below (see lines 30-36)
#
#

import subprocess
import os
import time
import psutil
from optparse import OptionParser

def ppip_core():
    parser = OptionParser(prog="ppip", version="%prog 0.1", usage="%prog [options]")
    parser.add_option('-p', action="store", type="string", dest="params",
                      help="opens params.txt file\n")
    parser.add_option('-s', action="store", dest="steps",
                      help="""perform step-wise parts of within analysis\n
                      1 = initial setup step                  	\
                      2 = assembly, N50, RSEM-EVAL             	\
                      3 = parse longest isoform, BUSCO         	\
                      4 = OrthoFinder, make gene trees, SSIMUL 	\
                      5 = treeCl								\
                      6 = modifying treeCl outputs				\
					  7 = MRP""" )


    (options, args) = parser.parse_args()

    if not any([options.params]):
        print "\n\tmust include -p to input params\n"
        sys.exit()

	if options.params:
		print("####Launching PPIP 0.1####")
		readin = [line.strip().split('##')[0].strip() for line in open(options.params).readlines()]
		
		
		##Read params.txt##
		name = str(readin[1])
		data_source = str(readin[2])
		cpu = int(readin[3])
		mem = str(readin[4])
		trinity_path = str(readin[5])
		detonate_path = str(readin[6])
		busco_path = str(readin[7])
		busco_data = str(readin[8])
		OrthoFinder_path = str(readin[9])    
		clustmax = int(readin[10])
		total_cpu = str(readin[11])
		cluster = int(readin[12])

		
		##Setup - subset of full pipeline##
		k = tuple('1234567')
		if options.steps:
			k = tuple(str(options.steps))
		
		
		##Setup - check to make sure enough CPU rerources available##
		##Removed free RAM check since most servers severely under-report due to caching##
		percent_cpu=psutil.cpu_percent()
		while(100-percent_cpu<(cpu/total_cpu)):
			time.sleep(30) #wait for 30secs, then check again		
		
		
		########Analysis pipeline starts here########
		
		if '1' in k:
			subprocess.call(['mkdir', 'bin']) #make folders required by pipeline
			subprocess.call(['mkdir', 'data_in'])
			subprocess.call(['mkdir', 'data_out'])
			subprocess.call(['mkdir', 'tmp'])
			print (">> mkdir {bin,data_in,data_out,reads,tmp}")
			os.chdir(os.path.abspath('data_out')) #make folder for annotated transcriptomes
			subprocess.call(['mkdir', 'annotated_transcriptomes']) #this is needed for step 4
			os.chdir(os.path.abspath('..'))
			
			os.chdir(os.path.abspath('bin'))
			print (">> cd /bin")
			subprocess.call(['ln', '-s', trinity_path, 'trinity']) #symlink trinity folder in /bin/
			print(">> ln -s trinity_path .")					
			subprocess.call(['ln', '-s', detonate_path, 'detonate']) #symlink DETONATE folder in /bin/
			print(">> ln -s DETONATE_path .")
			os.chdir(os.path.abspath('..'))
			print (">> cd ..")
			
			subprocess.call(['ln', '-s', data_source, 'reads']) #symlink reads (both fwd and rev)
			print (">> ln -s data_source reads")
			
			command= 'cat reads/*.fq_1 >> tmp/%s_forward.fq' % name
			subprocess.call(command, shell=True) #concatenate fwd reads
			print(">> %s" % command)
			command= 'cat reads/*.fq_2 >> tmp/%s_reverse.fq' % name
			subprocess.call(command, shell=True) #concatenate rev reads
			print(">> %s" % command)
			
			print ("***S1 Complete***")
		
		
		if '2' in k:
			fwd='./tmp/%s_forward.fq' % name
			rev='./tmp/%s_reverse.fq' % name
			print(fwd, rev)
			
			##Run Trinity##
			command='perl ./bin/trinity/Trinity --seqType fq --max_memory %s --left %s --right %s --SS_lib_type FR --CPU %d --normalize_reads --output data_out/trinity_out_%s --full_cleanup' % (mem,fwd,rev,cpu,name)
			print(">> %s" % command)
			subprocess.call(command, shell=True)
			
			##Calc N50##
			N50 = open('N50.txt', 'w')
			command= 'perl ./bin/trinity/util/TrinityStats.pl data_out/trinity_out_%s.Trinity.fasta' % name
			print(">> %s" % command)
			subprocess.Popen([command], shell=True, stdout=N50) #calculate basic N50 stats
			N50.close()
			command= 'mv N50.txt data_out'
			subprocess.call(command, shell=True)
			print(">> %s" % command)

			transcriptome='./data_out/trinity_out_%s.Trinity.fasta' % name
			subprocess.call(['mkdir', 'data_out/detonate_out'])

			##RSEM-EVAL##
			command= './bin/detonate/rsem-eval/rsem-eval-estimate-transcript-length-distribution %s %s_lengthdist.txt' % (transcriptome,name)
			print(">> %s" % command)
			subprocess.call(command, shell=True)

			command= './bin/detonate/rsem-eval/rsem-eval-calculate-score --num-threads %d --transcript-length-parameters %s_lengthdist.txt --strand-specific --paired-end %s %s %s %s 200' % (cpu,name,fwd,rev,transcriptome,name)
			print(">> %s" % command)
			subprocess.call(command, shell=True)

			command="mv %s* ./data_out/detonate_out" % name
			subprocess.Popen([command], shell=True)
			
			print ("***S2 Complete***")
			
			
		if '3' in k:
			try: from Bio import SeqIO
			except ImportError:
				print "\nError: Cannot import Biopython; check installation"
				sys.exit()
			
			##Parse Longest Isoform##
			wd = '/data_out/'
			dict_sequences = {}
			sequences = []
			genome = open(wd+'trinity_out_%s.Trinity.fasta','r') %name
			d = 0
		
			print('check')
		
			for contig in SeqIO.parse(genome, "fasta") :
				prev = id

			id = str(contig.description.split("_i")[0])
			
			if id not in dict_sequences or len(dict_sequences[id].seq) < len(contig.seq):
				try:
					dict_sequences[id] = contig
					d += 1
				except KeyError:
					dict_sequences[id] = [contig]
					
			print("Found %i sequences") % len(dict_sequences)
			print("Found %i duplicates") % d    
		
			for key, value in dict_sequences.iteritems():
				temp = value
				sequences.append(temp) 
		
			output_handle = open(wd+"%s_Trinity_clean.fa", "w") % name
			SeqIO.write(sequences, output_handle, "fasta")
			output_handle.close()
			
			##Modify Headers for BUSCO##
			import sys, getopt

			id = "%s_Trinity_clean.fasta" % name

			file_in = open(wd+id,'r')
			file_out = open(wd+id+'.headerfixed.fa','w')

			for line in file_in:
				if line[0] =='>':
					line=line.replace(':','')
					line=line.replace(' ','')
					line=line.replace('|','')
					line=line.replace('=','')
					line=line.replace('+','')
					line=line.replace('(','') 
					line=line.replace(')','')   
					file_out.write(line[0:20]+'\n')
				else:
					file_out.write(line)
			
			##Run Busco on Longest Isoforms Only Dataset##
			os.chdir(os.path.abspath('bin'))
			print (">> cd /bin")
			subprocess.call(['ln', '-s', busco_path, '.']) #symlink busco folder in /bin/
			print(">> ln -s busco_path .")
			os.chdir(os.path.abspath('..'))
			print (">> cd ..")
			
			os.chdir(os.path.abspath('data_out'))
			print (">> cd data_out")
			command= 'python3 %s -in %s_Trinity_clean.fasta.headerfixed.fa -l %s -o %s -m trans -f -c %d' % (busco_path, name, busco_data, name, cpu)
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
			
			##Run Busco on Full Trinity Assembly##
			command= 'python3 %s -in %s_Trinity_clean.fasta -l %s -o %s_full -m trans -f -c %d' % (busco_path, name, busco_data, name, cpu)
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
			os.chdir(os.path.abspath('..'))
			print(">> cd ..")
			
			print ("***S3 Complete***")
			
			
		if '4' in k:
			##Run OrthoFinder##
			#if len(os.listdir(data_out/annotated_transcriptomes)) > 0: #removed checking step, for now
			command= 'python %s/orthofinder.py -f data_out/annotated_transcriptomes -t %d' % (OrthoFinder_path , cpu)
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
				
			md= time.strftime("%b%d") #this is needed b/c OrthoFinder appends the date automatically to the results folder
			command= 'python %s/trees_for_orthogroups.py data_out/annotated_transcriptomes/Results_%s -t %d' % (OrthoFinder_path , md, cpu)
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
				
			#else: 
				#print("Error: Annotated transcriptomes are not in data_out/annotated_transcriptomes") #removed checking step, for now
				#sys.exit() #removed checking step, for now
			
			command= 'cat data_out/annotated_transcriptomes/Results_%s/Trees/*_tree.txt >> data_out/trees.txt'
			subprocess.call(command, shell=True) #move concatenated trees to data_out/
			print(">> %s" % command)
			
			##Run SSIMUL##
			
			command= 'isomorphism -s data_out/trees.txt'
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
			
			command= 'pruning -s data_out/MUL_trees.txt'
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
			
			##Take useable filtered trees and combne them in filtered_trees.txt for step 5#
			command= 'cat notMUL_trees.txt notMULAfter_trees.txt Pruned_trees.txt PrunedSubTrees_trees.txt >> filtered_trees.txt'
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
			print ("***S4 Complete***")
		
		
		if '5' in k:
			try: import treeCl
			except ImportError:
				print "\nError: Cannot import treeCl; check installation"
				sys.exit()
			
			##Run treeCl##
			from scipy.spatial.distance import squareform
			treeslist= open("data_out/filtered_trees.txt")
			clustmax= clustmax+1
			
			# Set up a tree-distance calculator; can select:
			# Geodesic-, RobinsonFoulds-, Euclidean-, WeightedRobinsonFoulds-
			distance_calculator = treeCl.tasks.GeodesicTreeDistance()

			# Set up a handler to run calculations)
			job_handler = treeCl.parutils.ProcesspoolJobHandler(int(cpu))

			# Generate list of arguments to measure distance between all pairs
			print("Generating list of args...")
			args = distance_calculator.scrape_args(treeslist)

			# Run calculations. Increased batchsize to 100 to make
			# parallel execution more efficient (less comm. overhead)
			print("Calculating distance array...")
			distance_array = job_handler(distance_calculator.get_task(), args, message='progress...', batchsize=100)

			# Convert to DistanceMatrix object
			print("Making DM...")
			dm = treeCl.DistanceMatrix.from_array(squareform(distance_array))
			dm.to_csv('data_out/treeCl/geo_dm_out.csv')

			#Clustering step
			print("Starting hierarchical clustering...")
			hclust = treeCl.Hierarchical(dm)

			for i in range(1, clustmax):
				partition = hclust.cluster(i)
				group_members = partition.get_membership()
				partition.write(data_out/treeCl/str(i) + '_Hclust.txt')

			print("Starting MDS clustering...")
			print("This may take a while... ~1.5h per cluster")
			mdsclust = treeCl.MultidimensionalScaling(dm)

			for i in range(1, clustmax):
				partition = mdsclust.cluster(i)
				group_members = partition.get_membership()
				partition.write(data_out/treeCl/str(i) + '_MDSclust.txt')

			print ("***S5 Complete***")
			
			
		if '6' in k:
			##Clean and parse treeCl results##
			##Here, we use the MDSclustered data, but you can replace MDSclust w/ Hclust##
			wd='data_out/treeCl/'
			
			#convert clusters from row to column#
			for i in range(1, clustmax):
				file_in = open(wd+ str(i) + '_MDSclust.txt', 'r')
				file_out = open(wd+ str(i)+ '_MDSclust_col.txt', 'w')

				for line in file_in:
					line=line.replace(',','\n')
					file_out.write(line)
			
			#make table from clusters + original dataset#
			for i in range(1, clustmax):
				command="paste data_out/treeCl/+str(i)+_MDSclust_col.txt filtered_trees.txt | column -s $'\t' -t >> data_out/treeCl/+str(i)+_MDSclust_table.txt"
				print(">> %s" % command)
				subprocess.Popen([command], shell=True)
			
			
			#sort tables by clusters#
			for i in range(1, clustmax):
					command = "sort -t $'\t' -k 1,1 data_out/treeCl/+ str(i)+ _MDSclust_table.txt >> data_out/treeCl/+str(i)+_MDSclust_table_sorted.txt"
					print(">> %s" % command)
					subprocess.Popen([command], shell=True)				
			
			#seperate tables - makes file *_split-XX where XX is the cluster number#
			for i in range(1, clustmax) and j in range(1,clustmax):
				command = "awk -F '\t' '{ print > (str(i)+_MDSclust_split- $1 .phy) }' data_out/treeCl/+str(i)+_MDSclust_table_sorted.txt"
				print(">> %s" % command)
				subprocess.Popen([command], shell=True)
			
			command = "awk '{print $2}' data_out/treeCl/*MDSclust_split-*.phy"
			print(">> %s" % command)
			subprocess.Popen([command], shell=True)
			
			
		if '7' in k:
			##Do MRP on the seperated tables from step 6##
			try: import p4
			except ImportError:
				print "\nError: Cannot import p4; check installation"
				sys.exit()
				
			import p4.mrp


			while i < len(int(cluster)+1):
				p4.read('%s+_MDSclust_split-+str(i).phy') % cluster
				a = p4.mrp.mrp(p4.var.trees)
				a.writeNexus('%s+_MDSclust_split-+str(i).nex') % cluster
			
if __name__ == "__ppip_core__":
	main()